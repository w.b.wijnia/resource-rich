-- Copyright 2007 Gas Powered Games - All rights reserved
--
-- Author: Matt Mahon
--
-- Description:
--    Sample Mod to double economic output of econ units.
--

--this function is called already in game. This gives us a hook to screw the 
--blueprint values for the units

do
    local oldModBlueprints = ModBlueprints

    function ModBlueprints(all_bps)
	    oldModBlueprints(all_bps)

        local econScales = { 2.0, 2.5, 3.0, 4.0, 5.0}
        local econScale = econScales[PreGameData.PreloadedOptions.ResourceRichMultiplier]
        
        --loop through the blueprints and adjust as desired.
        for id,bp in all_bps.Unit do
            if bp.Economy.ProductionPerSecondMass then
               bp.Economy.ProductionPerSecondMass = bp.Economy.ProductionPerSecondMass * econScale
            end
            if bp.Economy.ProductionPerSecondEnergy then
               bp.Economy.ProductionPerSecondEnergy = bp.Economy.ProductionPerSecondEnergy * econScale
            end  
        end
    end
end