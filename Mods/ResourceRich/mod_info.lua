name = "Resource Rich"
version = 1
copyright = "Copyright � 2006, Gas Powered Games"
description = "Increases resource production."
author = "Gas Powered Games"
url = "http://www.gaspoweredgames.com"
uid = "74A9EAB2-E851-11DB-A1F1-F2C755D89592"

exclusive = false

ui_only = false

