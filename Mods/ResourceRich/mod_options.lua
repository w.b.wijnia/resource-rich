options =
{
	{
        preload     = true,
		default 	= 1,
		label 		= "Multiplier",
		help 		= "The multiplier that is used to make the world more rich!",
		key 		= 'ResourceRichMultiplier',
		pref	 	= 'ResourceRichMultiplier',
		values 		= {
			{ 
                text = "2x", 
                help = "All production buildings will produce 2x the resources.", 
                key = 1, 
            },		
			{ 
                text = "2.5x", 
                help = "All production buildings will produce 2.5x the resources.", 
                key = 2, 
            },	
            { 
                text = "3x", 
                help = "All production buildings will produce 3x the resources.", 
                key = 3, 
            },	
            { 
                text = "4x", 
                help = "All production buildings will produce 4x the resources.", 
                key = 4, 
            },	
            { 
                text = "5x", 
                help = "All production buildings will produce 5x the resources.", 
                key = 5, 
            },		
		},	
    },
}